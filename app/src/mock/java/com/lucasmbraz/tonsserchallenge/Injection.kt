package com.lucasmbraz.tonsserchallenge

import com.lucasmbraz.tonsserchallenge.data.FakeFollowersRepository
import com.lucasmbraz.tonsserchallenge.data.FollowersRepository
import io.reactivex.Single

/**
 * Enables injection of mock implementations
 */
object Injection {

    var fakeRepository = FakeFollowersRepository(Single.just(emptyList()))

    fun provideFollowersRepository(): FollowersRepository = fakeRepository
}