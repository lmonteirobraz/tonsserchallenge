package com.lucasmbraz.tonsserchallenge.data

import android.util.Log
import com.lucasmbraz.tonsserchallenge.model.User
import com.lucasmbraz.tonsserchallenge.network.TonsserApi
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

interface FollowersRepository {
    fun getFollowers(lastSeenSlug: String): Single<List<User>>
}

class ProductionFollowersRepository(private val tonsserApi: TonsserApi) : FollowersRepository {
    override fun getFollowers(lastSeenSlug: String): Single<List<User>> {
        Log.d("Pagination", "Fetching page for last seem $lastSeenSlug")
        return tonsserApi.getFollowers(lastSeenSlug)
                .map { it.response }
                .subscribeOn(Schedulers.io())
    }
}