package com.lucasmbraz.tonsserchallenge

import com.lucasmbraz.tonsserchallenge.data.FollowersRepository
import com.lucasmbraz.tonsserchallenge.data.ProductionFollowersRepository
import com.lucasmbraz.tonsserchallenge.network.TonsserApi
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object Injection {

    private val tonsserApi by lazy {
        Retrofit.Builder()
                .baseUrl("http://staging-api.tonsser.com")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(TonsserApi::class.java)
    }

    fun provideFollowersRepository(): FollowersRepository
            = ProductionFollowersRepository(tonsserApi)
}