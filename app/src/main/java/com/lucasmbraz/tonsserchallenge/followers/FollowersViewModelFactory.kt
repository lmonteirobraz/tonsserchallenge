package com.lucasmbraz.tonsserchallenge.followers

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.lucasmbraz.tonsserchallenge.Injection

class FollowersViewModelFactory : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == FollowersViewModel::class.java) {
            val actionProcessor = FollowersActionProcessorHolder(Injection.provideFollowersRepository())
            return FollowersViewModel(actionProcessor) as T
        }
        return super.create(modelClass)
    }
}