package com.lucasmbraz.tonsserchallenge.followers

import com.lucasmbraz.tonsserchallenge.model.User
import com.lucasmbraz.tonsserchallenge.mvibase.MviViewState

data class FollowersViewState(
  val followers: List<User>,
  val isLoading: Boolean,
  val isLoadingMorePages: Boolean,
  val error: FollowersError?
) : MviViewState {

    enum class FollowersError { GENERIC, NO_FOLLOWERS }

    companion object {
        fun idle() = FollowersViewState(followers = emptyList(), isLoading = false, isLoadingMorePages = false, error = null)
    }
}