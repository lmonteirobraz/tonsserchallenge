package com.lucasmbraz.tonsserchallenge.mvibase

/**
 * Immutable object which contains all the required information to render an [MviView].
 */
interface MviViewState
