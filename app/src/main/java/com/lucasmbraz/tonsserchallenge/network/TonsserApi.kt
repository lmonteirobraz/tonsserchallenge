package com.lucasmbraz.tonsserchallenge.network

import com.lucasmbraz.tonsserchallenge.model.User
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface TonsserApi {
    @GET("51/users/christian-planck/followers")
    fun getFollowers(@Query("current_follow_slug") lastSeemSlug: String): Single<FollowersResponse>
}

data class FollowersResponse(val response: List<User>)