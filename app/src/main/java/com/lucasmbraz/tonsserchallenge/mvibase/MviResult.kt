package com.lucasmbraz.tonsserchallenge.mvibase

/**
 * Immutable object resulting of a processed business logic.
 */
interface MviResult
