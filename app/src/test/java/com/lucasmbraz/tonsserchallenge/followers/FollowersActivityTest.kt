package com.lucasmbraz.tonsserchallenge.followers

import android.os.Build
import android.view.View.VISIBLE
import com.lucasmbraz.tonsserchallenge.Injection
import com.lucasmbraz.tonsserchallenge.data.FakeFollowersRepository
import com.lucasmbraz.tonsserchallenge.model.User
import io.reactivex.Single
import kotlinx.android.synthetic.main.activity_followers.*
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.RuntimeException

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.LOLLIPOP])
class FollowersActivityTest {

    @Test fun showSadPath_whenNoFollowers() {
        val activity = Robolectric.setupActivity(FollowersActivity::class.java)

        assertEquals(VISIBLE, activity.sadPath.visibility)
        assertEquals("You don't have any followers", activity.sadPathTitle.text)
        assertEquals("Once people start following you, you'll see them here.", activity.sadPathMessage.text)
    }

    @Test fun showSadPath_whenException() {
        Injection.fakeRepository = FakeFollowersRepository(Single.error(RuntimeException()))

        val activity = Robolectric.setupActivity(FollowersActivity::class.java)

        assertEquals(VISIBLE, activity.sadPath.visibility)
        assertEquals("Oh no!", activity.sadPathTitle.text)
        assertEquals("Something went wrong. Please try again later.", activity.sadPathMessage.text)
    }

    @Test fun showFollowers() {
        val followers = listOf(User(slug = "", firstName = "John", lastName = "Smith", picture = ""))
        Injection.fakeRepository = FakeFollowersRepository(Single.just(followers))

        val activity = Robolectric.setupActivity(FollowersActivity::class.java)
        val recyclerView = activity.recyclerView

        assertEquals(VISIBLE, recyclerView.visibility)
        assertEquals(2, recyclerView.adapter?.itemCount)
        val adapter = recyclerView.adapter as FollowersAdapter
        assertEquals(followers, adapter.followers)
    }
}