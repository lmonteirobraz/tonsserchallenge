package com.lucasmbraz.tonsserchallenge

import com.google.common.truth.Truth.assertThat
import org.junit.Test

import java.time.LocalDate
import java.time.ZoneId
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class FindClosestEventTest {
    @Test
    fun findClosestEvent_isCorrect() {
        val sampleMatches = listOf(
            Match(slug = "1", date = LocalDate.of(2018, 5, 28).toDate(), homeTeamName = "A", awayTeamName = "B"),
            Match(slug = "2", date = LocalDate.of(2018, 6, 17).toDate(), homeTeamName = "C", awayTeamName = "D"),
            Match(slug = "3", date = LocalDate.of(2018, 6, 23).toDate(), homeTeamName = "E", awayTeamName = "F"),
            Match(slug = "4", date = LocalDate.of(2018, 6, 27).toDate(), homeTeamName = "G", awayTeamName = "H"),
            Match(slug = "5", date = LocalDate.of(2018, 6, 30).toDate(), homeTeamName = "I", awayTeamName = "J")
        )

        val dateClicked = LocalDate.of(2018, 5, 25).toDate()
        assertThat(findClosestEvent(dateClicked, emptyList())).isEqualTo(0)
        assertThat(findClosestEvent(dateClicked, listOf(sampleMatches.first()))).isEqualTo(0)

        assertThat(findClosestEvent(dateClicked, listOf(sampleMatches[0], sampleMatches[1]))).isEqualTo(0)
        assertThat(findClosestEvent(LocalDate.of(2018, 6, 25).toDate(), listOf(sampleMatches[0], sampleMatches[1]))).isEqualTo(1)

        assertThat(findClosestEvent(LocalDate.of(2018, 6, 17).toDate(), sampleMatches)).isEqualTo(1)

        assertThat(findClosestEvent(LocalDate.of(2018, 6, 1).toDate(), sampleMatches)).isEqualTo(0)
        assertThat(findClosestEvent(LocalDate.of(2018, 6, 18).toDate(), sampleMatches)).isEqualTo(1)

        assertThat(findClosestEvent(LocalDate.of(2018, 6, 15).toDate(), sampleMatches)).isEqualTo(1)
        assertThat(findClosestEvent(LocalDate.of(2018, 6, 22).toDate(), sampleMatches)).isEqualTo(2)
    }
}

fun LocalDate.toDate(): Date = Date.from(atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())

data class Match(val slug: String, val date: Date, val homeTeamName: String, val awayTeamName: String)

/**
 *
 * Return the closest match index to [dateClicked], return 0 otherwise, [matchList] is sorted by date.
 *
 * @return index of [matchList] closest event
 */
private fun findClosestEvent(dateClicked: Date, matchList: List<Match>): Int {
    if (matchList.isEmpty()) return 0

    val binarySearchResult = binarySearch(dateClicked, matchList)
    val lastIndex = binarySearchResult.lastIndex
    if (binarySearchResult.found) {
        return lastIndex
    } else {
        if (lastIndex == 0 || lastIndex == matchList.size - 1) return lastIndex

        val lastIndexDate = matchList[lastIndex].date
        if (dateClicked > lastIndexDate) {
            val diff1 = dateClicked.time - lastIndexDate.time
            val diff2 = matchList[lastIndex + 1].date.time - dateClicked.time

            return if (diff1 < diff2) lastIndex else lastIndex + 1
        } else {
            val diff1 = lastIndexDate.time - dateClicked.time
            val diff2 = dateClicked.time - matchList[lastIndex - 1].date.time

            return if (diff1 < diff2) lastIndex else lastIndex - 1
        }
    }
}

private fun binarySearch(dateClicked: Date, matchList: List<Match>): BinarySearchResult {
    var found = false

    var midPoint = -1
    var left = 0
    var right = matchList.size - 1

    while (left <= right && !found) {
        midPoint= (left + right) / 2
        val midPointDate = matchList[midPoint].date
        when {
            dateClicked < midPointDate -> right = midPoint - 1
            dateClicked > midPointDate -> left = midPoint + 1
            else -> found = true
        }
    }

    return BinarySearchResult(found, midPoint)
}

data class BinarySearchResult(val found: Boolean, val lastIndex: Int)
