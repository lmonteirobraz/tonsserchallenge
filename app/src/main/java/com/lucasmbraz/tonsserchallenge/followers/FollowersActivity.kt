package com.lucasmbraz.tonsserchallenge.followers

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View.GONE
import android.view.View.VISIBLE
import com.lucasmbraz.tonsserchallenge.R
import com.lucasmbraz.tonsserchallenge.followers.FollowersViewState.FollowersError
import com.lucasmbraz.tonsserchallenge.followers.FollowersViewState.FollowersError.*
import com.lucasmbraz.tonsserchallenge.model.User
import com.lucasmbraz.tonsserchallenge.mvibase.MviIntent
import com.lucasmbraz.tonsserchallenge.mvibase.MviView
import com.lucasmbraz.tonsserchallenge.mvibase.MviViewModel
import com.lucasmbraz.tonsserchallenge.mvibase.MviViewState
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_followers.*

class FollowersActivity : AppCompatActivity(), MviView<FollowersIntent, FollowersViewState> {

    private val loadMorePublisher = PublishSubject.create<FollowersIntent.LoadMoreIntent>()
    private val disposables = CompositeDisposable()

    private lateinit var adapter: FollowersAdapter
    private lateinit var layoutManager: LinearLayoutManager

    private val onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val totalCount = layoutManager.itemCount
            val visibleItemsCount = layoutManager.childCount
            val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
            if (lastVisibleItemPosition + visibleItemsCount >= totalCount) {
                val lastSeenSlug = adapter.followers.last().slug
                loadMorePublisher.onNext(FollowersIntent.LoadMoreIntent(lastSeenSlug))
            }
        }
    }

    private val viewModel: FollowersViewModel by lazy {
        ViewModelProviders.of(this, FollowersViewModelFactory()).get(FollowersViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_followers)

        layoutManager = LinearLayoutManager(this@FollowersActivity)
        adapter = FollowersAdapter()

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = this@FollowersActivity.layoutManager
            adapter = this@FollowersActivity.adapter
            addOnScrollListener(onScrollListener)
        }
    }

    override fun onStart() {
        super.onStart()
        bind()
    }

    /**
     * Connect the [MviView] with the [MviViewModel]
     * We subscribe to the [MviViewModel] before passing it the [MviView]'s [MviIntent]s.
     * If we were to pass [MviIntent]s to the [MviViewModel] before listening to it,
     * emitted [MviViewState]s could be lost
     */
    private fun bind() {
        // Subscribe to the ViewModel and call render for every emitted state
        disposables.add(viewModel.states().subscribe(this::render))
        // Pass the UI's intents to the ViewModel
        viewModel.processIntents(intents())
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    override fun intents(): Observable<FollowersIntent> {
        return Observable.merge(
            initialIntent(),
            loadMoreIntent()
        )
    }

    /**
     * The initial Intent the [MviView] emit to convey to the [MviViewModel]
     * that it is ready to receive data.
     * This initial Intent is also used to pass any parameters the [MviViewModel] might need
     * to render the initial [MviViewState] (e.g. the task id to load).
     */
    private fun initialIntent() = Observable.just(FollowersIntent.InitialIntent)

    private fun loadMoreIntent(): Observable<FollowersIntent.LoadMoreIntent>
            = loadMorePublisher.distinct()

    override fun render(state: FollowersViewState) {
        renderProgressIndicator(state.isLoading)
        renderLoadingMorePages(state.isLoadingMorePages)
        renderSadPath(state.error)
        renderFollowers(state.followers)
    }

    private fun renderProgressIndicator(isLoading: Boolean) {
        progress.visibility = if (isLoading) VISIBLE else GONE
    }

    private fun renderLoadingMorePages(isLoading: Boolean) {
        adapter.showLoading = isLoading
    }

    private fun renderSadPath(error: FollowersError?) {
        if (error == null) {
            sadPath.visibility = GONE
        } else {
            sadPath.visibility = VISIBLE
            when (error) {
                GENERIC -> {
                    sadPathImage.setImageResource(R.drawable.red_card)
                    sadPathTitle.setText(R.string.sad_path_generic_title)
                    sadPathMessage.setText(R.string.sad_path_generic_message)
                }
                NO_FOLLOWERS -> {
                    sadPathImage.setImageResource(R.drawable.no_followers)
                    sadPathTitle.setText(R.string.sad_path_no_followers_title)
                    sadPathMessage.setText(R.string.sad_path_no_followers_message)
                }
            }
        }
    }

    private fun renderFollowers(followers: List<User>) {
        recyclerView.visibility = if (followers.isNotEmpty()) VISIBLE else GONE
        adapter.updateData(followers)
    }
}
