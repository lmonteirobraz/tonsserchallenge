package com.lucasmbraz.tonsserchallenge.data

import com.lucasmbraz.tonsserchallenge.model.User
import io.reactivex.Single

class FakeFollowersRepository(private val response: Single<List<User>>) : FollowersRepository {

    override fun getFollowers(lastSeenSlug: String) = response
}