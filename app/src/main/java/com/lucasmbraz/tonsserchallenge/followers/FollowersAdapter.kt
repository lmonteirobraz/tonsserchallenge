package com.lucasmbraz.tonsserchallenge.followers

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.lucasmbraz.tonsserchallenge.R
import com.lucasmbraz.tonsserchallenge.model.User
import com.lucasmbraz.tonsserchallenge.utils.GlideApp
import kotlinx.android.synthetic.main.list_item_follower.view.*

class FollowersAdapter(var followers: List<User> = emptyList()) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEWTYPE_ITEM = 1
        const val VIEWTYPE_LOADING = 2
    }

    var showLoading = false
        set(value) {
            field = value
            notifyItemChanged(itemCount - 1)
        }

    class FollowerViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView by lazy { view.name }
        val image: ImageView by lazy { view.image }
    }

    class LoadingViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEWTYPE_ITEM -> {
                val view = inflater.inflate(R.layout.list_item_follower, parent, false)
                FollowerViewHolder(view)
            }
            VIEWTYPE_LOADING -> {
                val view = inflater.inflate(R.layout.list_item_loading_more, parent, false)
                LoadingViewHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid ViewType: $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position != 0 && position == itemCount - 1) {
            return VIEWTYPE_LOADING
        }
        return VIEWTYPE_ITEM
    }

    override fun getItemCount() = if (followers.isEmpty()) 0 else followers.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is FollowerViewHolder -> bindFollower(holder, position)
            is LoadingViewHolder -> bindLoadingIndicator(holder)
            else -> throw IllegalArgumentException("Invalid Holder type: $holder")
        }
    }

    private fun bindFollower(holder: FollowerViewHolder, position: Int) {
        val follower = followers[position]
        val context = holder.view.context
        holder.name.text = context.getString(R.string.follower_name, follower.firstName, follower.lastName)
        GlideApp.with(context)
                .load(follower.picture)
                .placeholder(R.drawable.user_image_placeholder)
                .error(R.drawable.user_image_placeholder)
                .circleCrop()
                .into(holder.image)
    }

    private fun bindLoadingIndicator(holder: LoadingViewHolder) {
        holder.view.visibility = if (showLoading) VISIBLE else GONE
    }

    fun updateData(followers: List<User>) {
        val diffResult = DiffUtil.calculateDiff(UserDiffCallback(this.followers, followers))
        this.followers = followers
        diffResult.dispatchUpdatesTo(this)
    }
}

class UserDiffCallback(
        private val oldUsers: List<User>,
        private val newUsers: List<User>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldUsers.size

    override fun getNewListSize() = newUsers.size

    override fun areItemsTheSame(oldPosition: Int, newPosition: Int)
            = oldUsers[oldPosition].slug == newUsers[newPosition].slug

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int)
            = oldUsers[oldPosition] == newUsers[newPosition]
}