package com.lucasmbraz.tonsserchallenge.model

import com.google.gson.annotations.SerializedName

data class User(
    val slug: String,
    @SerializedName("firstname") val firstName: String,
    @SerializedName("lastname") val lastName: String,
    @SerializedName("profile_picture") val picture: String
)