package com.lucasmbraz.tonsserchallenge.followers

import com.lucasmbraz.tonsserchallenge.model.User
import com.lucasmbraz.tonsserchallenge.mvibase.MviResult

sealed class FollowersResult : MviResult {
    sealed class LoadFollowersResult : FollowersResult() {
        data class Success(val followers: List<User>) : LoadFollowersResult()
        data class Failure(val error: Throwable) : LoadFollowersResult()
        object InFlight : LoadFollowersResult()
    }
}