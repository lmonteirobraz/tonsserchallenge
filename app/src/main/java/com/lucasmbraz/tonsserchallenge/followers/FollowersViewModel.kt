package com.lucasmbraz.tonsserchallenge.followers

import android.arch.lifecycle.ViewModel
import com.lucasmbraz.tonsserchallenge.followers.FollowersAction.LoadFollowersAction
import com.lucasmbraz.tonsserchallenge.followers.FollowersIntent.InitialIntent
import com.lucasmbraz.tonsserchallenge.followers.FollowersIntent.LoadMoreIntent
import com.lucasmbraz.tonsserchallenge.followers.FollowersResult.LoadFollowersResult
import com.lucasmbraz.tonsserchallenge.followers.FollowersViewState.FollowersError.GENERIC
import com.lucasmbraz.tonsserchallenge.followers.FollowersViewState.FollowersError.NO_FOLLOWERS
import com.lucasmbraz.tonsserchallenge.mvibase.*
import com.lucasmbraz.tonsserchallenge.utils.notOfType
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject

class FollowersViewModel(
        private val actionProcessorHolder: FollowersActionProcessorHolder
) : ViewModel(), MviViewModel<FollowersIntent, FollowersViewState> {

    /**
     * Proxy subject used to keep the stream alive even after the UI gets recycled.
     * This is basically used to keep ongoing events and the last cached State alive
     * while the UI disconnects and reconnects on config changes.
     */
    private val intentsSubject: PublishSubject<FollowersIntent> = PublishSubject.create()
    private val statesObservable: Observable<FollowersViewState> = compose()

    /**
     * take only the first ever InitialIntent and all intents of other types
     * to avoid reloading data on config changes
     */
    private val intentFilter: ObservableTransformer<FollowersIntent, FollowersIntent>
        get() = ObservableTransformer { intents ->
            intents.publish { shared ->
                Observable.merge(
                    shared.ofType(FollowersIntent.InitialIntent::class.java).take(1),
                    shared.notOfType(FollowersIntent.InitialIntent::class.java)
                )
            }
        }

    override fun processIntents(intents: Observable<FollowersIntent>) {
        intents.subscribe(intentsSubject)
    }

    override fun states(): Observable<FollowersViewState> = statesObservable

    /**
     * Compose all components to create the stream logic
     */
    private fun compose(): Observable<FollowersViewState> {
        return intentsSubject
            .compose(intentFilter)
            .map(this::actionFromIntent)
            .compose(actionProcessorHolder.actionProcessor)
            // Cache each state and pass it to the reducer to create a new state from
            // the previous cached one and the latest Result emitted from the action processor.
            // The Scan operator is used here for the caching.
            .scan(FollowersViewState.idle(), reducer)
            // When a reducer just emits previousState, there's no reason to call render. In fact,
            // redrawing the UI in cases like this can cause jank (e.g. messing up snackbar animations
            // by showing the same snackbar twice in rapid succession).
            .distinctUntilChanged()
            // Emit the last one event of the stream on subscription
            // Useful when a View rebinds to the ViewModel after rotation.
            .replay(1)
            // Create the stream on creation without waiting for anyone to subscribe
            // This allows the stream to stay alive even when the UI disconnects and
            // match the stream's lifecycle to the ViewModel's one.
            .autoConnect(0)
    }

    /**
     * Translate an [MviIntent] to an [MviAction].
     * Used to decouple the UI and the business logic to allow easy testings and reusability.
     */
    private fun actionFromIntent(intent: FollowersIntent): FollowersAction {
        return when (intent) {
            is InitialIntent -> LoadFollowersAction("")
            is LoadMoreIntent -> LoadFollowersAction(intent.lastSeenSlug)
        }
    }

    companion object {
        /**
         * The Reducer is where [MviViewState]s, that the [MviView] will use to
         * render itself, are created.
         * It takes the last cached [MviViewState], the latest [MviResult] and
         * creates a new [MviViewState] by only updating the related fields.
         * This is basically like a big switch statement of all possible types for the [MviResult]
         */
        private val reducer = BiFunction { previousState: FollowersViewState, result: FollowersResult ->
            when (result) {
                is LoadFollowersResult -> when (result) {
                    is LoadFollowersResult.InFlight -> previousState.copy(isLoading = previousState.followers.isEmpty(), isLoadingMorePages = previousState.followers.isNotEmpty())
                    is LoadFollowersResult.Failure -> previousState.copy(error = GENERIC, isLoading = false, isLoadingMorePages = false)
                    is LoadFollowersResult.Success -> {
                        if (result.followers.isNotEmpty()) {
                            val combinedFollowers = previousState.followers + result.followers
                            previousState.copy(followers = combinedFollowers, isLoading = false, isLoadingMorePages = false, error = null)
                        } else {
                            // Show sad path
                            previousState.copy(error = NO_FOLLOWERS, isLoading = false, isLoadingMorePages = false)
                        }
                    }
                }
            }
        }
    }
}