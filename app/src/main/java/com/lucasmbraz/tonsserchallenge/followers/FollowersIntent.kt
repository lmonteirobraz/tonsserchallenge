package com.lucasmbraz.tonsserchallenge.followers

import com.lucasmbraz.tonsserchallenge.mvibase.MviIntent

sealed class FollowersIntent : MviIntent {
    object InitialIntent : FollowersIntent()

    data class LoadMoreIntent(val lastSeenSlug: String) : FollowersIntent()
}