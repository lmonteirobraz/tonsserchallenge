package com.lucasmbraz.tonsserchallenge.mvibase

/**
 * Immutable object which represent a view's intent.
 */
interface MviIntent
