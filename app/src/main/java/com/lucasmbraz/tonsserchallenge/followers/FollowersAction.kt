package com.lucasmbraz.tonsserchallenge.followers

import com.lucasmbraz.tonsserchallenge.mvibase.MviAction

sealed class FollowersAction : MviAction {
    data class LoadFollowersAction(val lastSeenSlug: String) : FollowersAction()
}